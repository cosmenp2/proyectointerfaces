﻿using LibreriaV5_1_Final.Persistencia;
using System;
using System.Collections.Generic;


namespace LibreriaV5_1_Final.Negocio
{
    public class ControlAccesoDAO<T> : IAcceso<T> where T : new()
    {
        AccesoDAO<T> accesoDAO = new AccesoDAO<T>();

        public bool BorradoVirtual(List<T> objetos)
        {
            try
            {
                return ((IAcceso<T>)accesoDAO).BorradoVirtual(objetos);
            }
            catch (Exception) { throw; }
        }

        public bool Borrar(List<T> objetos)
        {
            try
            {
                return ((IAcceso<T>)accesoDAO).Borrar(objetos);
            }
            catch (Exception) { throw; }
        }

        public bool Insertar(List<T> objetos)
        {
            try
            {
                return ((IAcceso<T>)accesoDAO).Insertar(objetos);
            }
            catch (Exception) { throw; }
        }

        public bool Modificar(List<T> objetos)
        {
            try
            {
                return ((IAcceso<T>)accesoDAO).Modificar(objetos);
            }
            catch (Exception) { throw; }
        }

        public List<object> Buscar(Object filtro)
        {
            try
            {
                return ((IAcceso<T>)accesoDAO).Buscar(filtro);
            }
            catch (Exception) { throw; }
        }

        public List<object> BuscarConPaginacion(Object filtro, int pagina)
        {
            try
            {
                return ((IAcceso<T>)accesoDAO).BuscarConPaginacion(filtro,pagina);
            }
            catch (Exception) { throw; }
        }

        public int ContarRegistros(Object filtro)
        {
            try
            {
                return ((IAcceso<T>)accesoDAO).ContarRegistros(filtro);
            }
            catch (Exception) { throw; }
        }

        public void CerrarAplicacion()
        {
            accesoDAO.CloseConnection();
        }
    }
}
