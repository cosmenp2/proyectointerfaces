﻿using System;
using System.Collections.Generic;

namespace LibreriaV5_1_Final.Persistencia
{
    interface IAcceso<obj>
    {
        bool Insertar(List<obj> objetos);
        bool Borrar(List<obj> objeto);
        bool BorradoVirtual(List<obj> objeto);
        List<object> Buscar(Object objeto);
        List<object> BuscarConPaginacion(Object objeto, int pagina);
        int ContarRegistros(Object objeto);
        bool Modificar(List<obj> objetos);
        
    }

}
