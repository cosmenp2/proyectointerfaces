﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace LibreriaV5_1_Final.Persistencia
{
    public class AccesoDAO<T> : AccesoBD, IAcceso<T> where T : new()
    {
        public bool BorradoVirtual(List<T> objetos)
        {
            /*
             Se sabe que se puede obtener una propiedad en concreto preguntado por su nombre, 
             pero esto modificaría el orden de como obtiene las propiedades en EjecutarUpdate. 
             El orden pasa de ser de “el orden en el que estén declarados los atributos de una clase Entity” a 
             “primero el atributo modificado, después el orden de los atributos declarados” 
             provocando fallos a la hora de insertar valores en la sql parametrizada.
             De esta forma obtenemos la lista completa y no modificamos su orden.
             */
            string sql;
            bool borrado = true;
            try
            {
                int index = 0;

                while (borrado && index < objetos.Count)
                {
                    borrado = false;
                    PropertyInfo[] properties = objetos[index].GetType().GetProperties();
                    properties[properties.Length - 1].SetValue(objetos[index], "1");
                    if ((sql = UtilFichero.ObtenerSentencia("BORRADOVIRTUAL" + objetos[index].GetType().Name)) == null)
                    {
                        sql = UtilSQL.SqlModificar(objetos[index]);
                        UtilFichero.GuardarSQL("BORRADOVIRTUAL" + objetos[index].GetType().Name, sql);
                    }

                    if (EjecutarUpdate(UtilSQL.ParametrizarUpdate(objetos[index], sql), index == objetos.Count - 1))
                    {
                        borrado = true;
                    }
                    index++;
                }
            }
            catch (Exception) { throw; }

            return borrado;
        }

        public bool Borrar(List<T> objetos)
        {
            string sql;
            bool borrado = true;
            try
            {
                int index = 0;

                while (borrado && index < objetos.Count)
                {
                    borrado = false;
                    if ((sql = UtilFichero.ObtenerSentencia("DELETE" + objetos[index].GetType().Name)) == null)
                    {
                        sql = UtilSQL.SqlBorrar(objetos[index]);
                        UtilFichero.GuardarSQL("DELETE" + objetos[index].GetType().Name, sql);
                    }
                    if (EjecutarUpdate(UtilSQL.ParametrizarUpdate(objetos[index], sql), index == objetos.Count - 1))
                    {
                        borrado = true;
                    }
                    index++;
                }
            }
            catch (Exception) { throw; }

            return borrado;
        }

        public List<Object> Buscar(Object objeto)
        {
            List<Object> obj = null;
            string sql;
            try
            {
                sql = UtilSQL.GenerarSql(objeto);
                
                obj = EjecutarConsulta(UtilSQL.ParametrizarConsulta(objeto, sql), objeto.GetType());
            }
            catch (Exception) { throw; }

            return obj;
        }

        public List<Object> BuscarConPaginacion(Object objeto, int pagina)
        {
            List<Object> obj = null;
            string sql;
            try
            {
                sql = UtilSQL.GenerarSql(objeto,pagina);

                obj = EjecutarConsulta(UtilSQL.ParametrizarConsulta(objeto, sql), objeto.GetType());
            }
            catch (Exception) { throw; }

            return obj;
        }

        public int ContarRegistros(Object objeto)
        {
            List<Object> obj = null;
            string sql;
            try
            {
                sql = UtilSQL.GenerarCount(objeto);
                int a = 0;
                obj = EjecutarConsulta(UtilSQL.ParametrizarConsulta(objeto, sql), a.GetType());
            }
            catch (Exception) { throw; }

            return (int)obj[0];
        }

        public bool Insertar(List<T> objetos)
        {
            bool insertado = true;
            string sql;

            try
            {
                int index = 0;
                while (insertado && index < objetos.Count)
                {

                    insertado = false;
                    if ((sql = UtilFichero.ObtenerSentencia("INSERTAR" + objetos[index].GetType().Name)) == null)
                    {
                        sql = UtilSQL.SqlInsertar(objetos[index]);
                        UtilFichero.GuardarSQL("INSERTAR" + objetos[index].GetType().Name, sql);
                    }
                    if (EjecutarUpdate(UtilSQL.ParametrizarUpdate(objetos[index], sql), index == objetos.Count - 1)) //Se hace commit si llega al final
                    {
                        insertado = true;
                    }
                    index++;
                }
            }
            catch (Exception) { throw; }
            return insertado;
        }

        public bool Modificar(List<T> objetos)
        {
            bool modificado = true;
            string sql;

            try
            {
                int index = 0;
                while (modificado && index < objetos.Count)
                {
                    modificado = true;
                    if ((sql = UtilFichero.ObtenerSentencia("UPDATE" + objetos[index].GetType().Name)) == null)
                    {
                        sql = UtilSQL.SqlModificar(objetos[index]);
                        UtilFichero.GuardarSQL("UPDATE" + objetos[index].GetType().Name, sql);
                    }
                    if (EjecutarUpdate(UtilSQL.ParametrizarUpdate(objetos[index], sql), index == objetos.Count - 1))
                    {
                        modificado = true;
                    }
                    index++;
                }
            }
            catch (Exception) { throw; }

            return modificado;
        }

        public string ObtenerCodigo(Type clase)
        {
            String codigo = "";
            try
            {
                string sql = "SELECT * FROM " + clase.Name.ToLower() + " WHERE Cod" + clase.Name.Substring(1) + " = (SELECT MAX(Cod" + clase.Name.Substring(1) + ") FROM " + clase.Name.ToLower() + ")";
                MySqlCommand comando = new MySqlCommand(sql);
                List<Object> resultados = EjecutarConsulta(comando, clase);
                if (resultados.Count != 0)
                {
                    PropertyInfo clave = clase.GetProperty(UtilSQL.ObtenerClave(clase));
                    codigo = clave.GetValue(resultados[0]).ToString();
                }
                else
                {
                    codigo = "Cod000";
                }
            }
            catch (Exception) { throw; }

            return codigo;
        }

    }
}
