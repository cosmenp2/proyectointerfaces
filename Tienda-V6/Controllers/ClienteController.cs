﻿using LibreriaV5_1_Final.Comun;
using LibreriaV5_1_Final.Modelo;
using LibreriaV5_1_Final.Negocio;
using LibreriaV5_1_Final.Persistencia;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Web.Mvc;

namespace Tienda_V6.Controllers
{
    public class ClienteController : Controller
    {
        private ControlAccesoDAO<Object> control = new ControlAccesoDAO<Object>();

        public ActionResult Consultar()
        {
            Object[] modelos = new Object[2];
            TCliente filtro = new TCliente();
            filtro.Borrado = "0";
            List<Object> clientes = control.BuscarConPaginacion(filtro, 1);
            modelos[0] = clientes;
            modelos[1] = control.ContarRegistros(filtro);
            return View(modelos);
        }

        public ActionResult ver(string cli)
        {
            String prueba;
            TCliente filtro = new TCliente();
            filtro.CodCliente = cli;
            TCliente cliente = (TCliente)control.Buscar(filtro)[0];
            return View("ver", cliente);
        }

        [HttpPost]
        public ActionResult buscarConPaginacion(int numeroPagina)
        {
            Object[] modelos = new Object[2];
            TCliente filtro = new TCliente();
            filtro.Borrado = "0";
            List<Object> clientes = control.BuscarConPaginacion(filtro, numeroPagina);
            modelos[0] = clientes;
            modelos[1] = control.ContarRegistros(filtro);
            return Json(modelos);
        }

        public ActionResult add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult add(TCliente cliente, TUsuario usuario)
        {

            String mensaje;
            try
            {
                usuario.CodUsuario = UtilSQL.GenerarCodigo(usuario.GetType());
                cliente.CodCliente = UtilSQL.GenerarCodigo(cliente.GetType());
                cliente.CodUsuario = usuario.CodUsuario;
                usuario.Rol = "Cliente";
                usuario.Pass = encriptar(usuario.Pass);    //Encripta la contraseña            
                List<Object> clienteUsuario = new List<Object>();

                clienteUsuario.Add(cliente);
                clienteUsuario.Add(usuario);
                control.Insertar(clienteUsuario);
                if (((TUsuario)Session["usuario"]) == null)
                {
                    Session["usuario"] = usuario;
                }
                else if (!((TUsuario)Session["usuario"]).Rol.Equals("Administrador"))
                {
                    Session["usuario"] = usuario;
                }
                mensaje = Mensajes.MSG_INSERTADO_CLIENTE;
            }
            catch (Exception e)
            {
                mensaje = "ERR" + e.ToString();
            }
            return Json(mensaje);
        }

        public ActionResult modificar(string cli)
        {
            TCliente cliente = new TCliente();
            TUsuario usuario = new TUsuario();
            cliente.CodUsuario = cli;
            usuario.CodUsuario = cli;
            ViewBag.cliente = (TCliente)control.Buscar(cliente)[0];
            ViewBag.usuario = (TUsuario)control.Buscar(usuario)[0];
            return View();
        }

        [HttpPost]
        public ActionResult modificar(TCliente cliente, TUsuario usuario)
        {
            String mensaje;
            try
            {
                TCliente filtro = new TCliente();
                TUsuario oldUser = new TUsuario();
                cliente.CodUsuario = ((TUsuario)Session["usuario"]).CodUsuario;
                oldUser.CodUsuario = cliente.CodUsuario;
                oldUser = (TUsuario)control.Buscar(oldUser)[0];
                oldUser.Nick = usuario.Nick;
                List<Object> clientesUsuarios = new List<Object>();
                clientesUsuarios.Add(cliente);
                clientesUsuarios.Add(oldUser);
                control.Modificar(clientesUsuarios);
                mensaje = Mensajes.MSG_MODIFICADO_CLIENTE;
            }
            catch (Exception e)
            {
                mensaje = "ERR" + e.ToString();
            }
            return Json(mensaje);
        }

        [HttpPost]
        public PartialViewResult borrar(string cli)
        {
            TCliente cliente = new TCliente();
            TUsuario usuario = new TUsuario();
            List<Object> clienteUsuario = new List<object>();
            cliente.CodCliente = cli;
            cliente = (TCliente)control.Buscar(cliente)[0];
            clienteUsuario.Add(cliente);
            control.BorradoVirtual(clienteUsuario);
            cliente = new TCliente();
            cliente.Borrado = "0";
            return PartialView("borrar", control.ContarRegistros(cliente));
        }

        public static string encriptar(string password)
        {
            SHA1CryptoServiceProvider provider = new SHA1CryptoServiceProvider();
            byte[] vectoBytes = System.Text.Encoding.UTF8.GetBytes(password);
            byte[] inArray = provider.ComputeHash(vectoBytes);
            provider.Clear();
            return Convert.ToBase64String(inArray);
        }

        [HttpPost]
        public ActionResult login(TUsuario usuario)
        {
            String mensaje;
            try
            {
                usuario.Pass = encriptar(usuario.Pass);
                List<Object> resultados = control.Buscar(usuario);
                if (resultados.Count == 1)
                {
                    TUsuario usuarioIniciado = (TUsuario)resultados[0];
                    if (usuarioIniciado != null)
                    {
                        Session["usuario"] = (TUsuario)resultados[0];
                        mensaje = Mensajes.MSG_LOGIN_EXITO;
                    }
                    else
                    {
                        mensaje = "ERR" + Mensajes.MSG_LOGIN_ROLERROR;
                    }

                }
                else
                {
                    mensaje = "ERR" + Mensajes.MSG_LOGIN_ERROR;
                }
            }
            catch (Exception e)
            {
                mensaje = "ERR" + e.ToString();
            }
            return Json(mensaje);
        }

        public ActionResult restablecer()
        {
            return View();
        }

        [HttpPost]
        public ActionResult restablecer(string pass1, string pass2, string oldPass)
        {
            String mensaje;
            if (pass2 == pass1)
            {
                try
                {
                    TUsuario usuario = new TUsuario();
                    usuario.Pass = encriptar(oldPass);
                    usuario.CodUsuario = ((TUsuario)Session["usuario"]).CodUsuario;
                    List<Object> resultados = control.Buscar(usuario);
                    if (resultados.Count == 1)
                    {
                        TUsuario modificado = (TUsuario)resultados[0];
                        modificado.Pass = encriptar(pass1);
                        List<Object> usuarios = new List<Object>();
                        usuarios.Add(modificado);
                        control.Modificar(usuarios);
                        mensaje = Mensajes.MSG_PASS_CAMBIADA;
                    }
                    else
                    {
                        mensaje = "ERR" + Mensajes.MSG_PASS_NO_CORRECTA;
                    }

                }
                catch (Exception e)
                {
                    mensaje = "ERR" + e.ToString();
                }
            }
            else
            {
                mensaje = "ERR" + Mensajes.MSG_PASS_NO_IGUALES;
            }
            return Json(mensaje);
        }
        public ActionResult cerrarSesion()
        {
            Session["usuario"] = null;
            return View();
        }


        [HttpPost]
        public ActionResult ComprobarExistenciaEmail(string email)
        {
            string mensaje = "<div class='alert alert-danger'><strong>Oh no!</strong> Email no disponible.</div>";
            TUsuario filtro = new TUsuario()
            {
                Nick = email
            };
            if (control.Buscar(filtro).Count == 0)
            {
                mensaje = "<div class='alert alert-success'><strong>Sii!</strong> Email disponible.</div>";

            }
            return Json(mensaje);
        }

        [HttpPost]
        public ActionResult ComprobarSession()
        {
            bool admin = true;
            if (((TUsuario)Session["usuario"]) == null)
            {
                admin = false;
            }
            return Json(admin);
        }
    }
}