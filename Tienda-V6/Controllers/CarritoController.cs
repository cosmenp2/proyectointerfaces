﻿using LibreriaV5_1_Final.Modelo;
using LibreriaV5_1_Final.Negocio;
using LibreriaV5_1_Final.Persistencia;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Mvc;

namespace Tienda_V6.Controllers
{
    public class CarritoController : Controller
    {
        private ControlAccesoDAO<Object> control = new ControlAccesoDAO<Object>();

        public ActionResult carrito()
        {
            return View();
        }

        [HttpPost]
        public ActionResult comprar(List<TLineaFactura> data)
        {
            string mensaje = "ERRHa ocurrido un error guardar la factura";
            try
            {
                TCliente cliente = new TCliente()
                {
                    CodUsuario = ((TUsuario)Session["usuario"]).CodUsuario
                };
                cliente = (TCliente)control.Buscar(cliente)[0];
                TFactura factura = new TFactura(UtilSQL.GenerarCodigo(new TFactura().GetType()), cliente.CodCliente, DateTime.Now.ToShortDateString());
                List<Object> listaTempAdd = new List<Object>() {
                factura
            };

                foreach (TLineaFactura linea in data)
                {
                    if (linea.Cantidad.ToString() == "0")
                    {
                        mensaje = "No se permiten cantidades 0";
                        return Json(mensaje);
                    }
                    TArticulo tmp = new TArticulo()
                    {
                        CodArticulo = linea.Articulo
                    };
                    tmp = (TArticulo)(control.Buscar(tmp)[0]);
                    ; double precio = double.Parse(tmp.Precio.Replace(',', '.'), CultureInfo.InvariantCulture);
                    TLineaFactura lineaTemp = new TLineaFactura(factura.CodFactura, linea.Articulo, linea.Cantidad.ToString(), "" + (precio * int.Parse(linea.Cantidad)));
                    listaTempAdd.Add(lineaTemp);
                }
                if (control.Insertar(listaTempAdd))
                {
                    mensaje = "Factura guardada correctamente";
                }
            }
            catch {

            }
            return Json(mensaje);
        }


    }
}