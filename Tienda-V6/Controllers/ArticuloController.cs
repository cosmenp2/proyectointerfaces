﻿using LibreriaV5_1_Final.Comun;
using LibreriaV5_1_Final.Modelo;
using LibreriaV5_1_Final.Negocio;
using LibreriaV5_1_Final.Persistencia;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Tienda_V6.Controllers
{
    public class ArticuloController : Controller
    {
        private ControlAccesoDAO<Object> control = new ControlAccesoDAO<Object>();

        public ActionResult Consultar()
        {
            Object[] modelos = new Object[2];
            TArticulo filtro = new TArticulo();
            filtro.Borrado = "0";
            List<Object> articulos = control.BuscarConPaginacion(filtro, 1);
            modelos[0] = articulos;
            modelos[1] = control.ContarRegistros(filtro);
            return View(modelos);
        }

        public ActionResult ver(string art)
        {
            TArticulo filtro = new TArticulo();
            filtro.CodArticulo = art;
            TArticulo articulo = (TArticulo)control.Buscar(filtro)[0];
            return View("ver", articulo);
        }

        [HttpPost]
        public ActionResult buscarConPaginacion(int numeroPagina)
        {
            Object[] modelos = new Object[2];
            TArticulo filtro = new TArticulo();
            filtro.Borrado = "0";
            List<Object> articulos = control.BuscarConPaginacion(filtro, numeroPagina);
            modelos[0] = articulos;
            modelos[1] = control.ContarRegistros(filtro);
            return Json(modelos);
        }

        public ActionResult add()
        {
            return View(control.Buscar(new TTipo()));
        }

        [HttpPost]
        public ActionResult add(TArticulo articulo)
        {
            String mensaje;
            try
            {
                //Hacemos la operaciones necesarias para guardar el nuevo libro.
                articulo.Precio = articulo.Precio.Replace(".", ",");
                articulo.CodArticulo = UtilSQL.GenerarCodigo(articulo.GetType());
                List<Object> articulos = new List<Object>();
                articulos.Add(articulo);
                control.Insertar(articulos);
                mensaje = Mensajes.MSG_INSERTADO_ARTICULO;
            }
            catch (Exception e)
            {
                mensaje = "ERR" + e.ToString();
            }
            return Json(mensaje);
        }

        public ActionResult modificar(string art)
        {
            TArticulo articulo = new TArticulo();
            articulo.CodArticulo = art;
            ViewBag.articulo = (TArticulo)control.Buscar(articulo)[0];
            return View(control.Buscar(new TTipo()));
        }

        [HttpPost]
        public ActionResult modificar(TArticulo articulo)
        {
            String mensaje;
            try
            {
                articulo.Precio = articulo.Precio.Replace(".", ",");
                List<Object> articulos = new List<Object>();
                articulos.Add(articulo);
                control.Modificar(articulos);
                mensaje = Mensajes.MSG_MODIFICADO_ARTICULO;
            }
            catch (Exception e)
            {
                mensaje = "ERR" + e.ToString();
            }
            return Json(mensaje);
        }

        [HttpPost]
        public PartialViewResult borrar(string art)
        {
            TArticulo filtro = new TArticulo();
            filtro.CodArticulo = art;
            control.BorradoVirtual(control.Buscar(filtro));
            filtro = new TArticulo();
            filtro.Borrado = "0";
            return PartialView("borrar", control.ContarRegistros(filtro));
        }

        public ActionResult compra()
        {
            List<TArticulo> list = new List<TArticulo>();
            foreach (var item in control.Buscar(new TArticulo()))
            {
                list.Add((TArticulo)item);
            }
            return View(list);
        }



        [HttpPost]
        public ActionResult obtener(string CodArticulo)
        {
            Object[] modelos = new Object[2];
            TArticulo filtro = new TArticulo()
            {
                CodArticulo = CodArticulo
            };
            filtro = (TArticulo)control.Buscar(filtro)[0];
            TLineaFactura linea = new TLineaFactura()
            {
                Articulo = CodArticulo,
                Cantidad = "1",
                Total = filtro.Precio
            };

            modelos[0] = filtro;
            modelos[1] = linea;
            // modelos[2] = control.ContarRegistros(filtro);
            return Json(modelos);
        }
    }
}
