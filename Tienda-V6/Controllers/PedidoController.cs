﻿using LibreriaV5_1_Final.Comun;
using LibreriaV5_1_Final.Modelo;
using LibreriaV5_1_Final.Negocio;
using LibreriaV5_1_Final.Persistencia;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Tienda_V6.Controllers
{
    public class PedidoController : Controller
    {
        private ControlAccesoDAO<Object> control = new ControlAccesoDAO<Object>();

        public ActionResult consultar()
        {
            Object[] modelos = new Object[2];
            TFactura filtro = new TFactura();
            if (!((TUsuario)Session["usuario"]).Rol.Equals("Administrador"))
            {
                TCliente cliente = new TCliente()
                {
                    CodUsuario = ((TUsuario)Session["usuario"]).CodUsuario
                };
                cliente = (TCliente)control.Buscar(cliente)[0];
                filtro.Cliente = cliente.CodCliente;
            }
            filtro.Borrado = "0";
            List<Object> facturas = control.Buscar(filtro);
            modelos[0] = facturas;
            modelos[1] = control.ContarRegistros(filtro);
            return View(modelos);
        }

        [HttpPost]
        public ActionResult consultarConPaginacion(int numeroPagina)
        {
            Object[] modelos = new Object[2];
            TFactura filtro = new TFactura();
            if (!((TUsuario)Session["usuario"]).Rol.Equals("Administrador"))
            {
                TCliente cliente = new TCliente()
                {
                    CodUsuario = ((TUsuario)Session["usuario"]).CodUsuario
                };
                cliente = (TCliente)control.Buscar(cliente)[0];
                filtro.Cliente = cliente.CodCliente;
            }
            filtro.Borrado = "0";
            List<Object> facturas = control.BuscarConPaginacion(filtro,numeroPagina);
            modelos[0] = facturas;
            modelos[1] = control.ContarRegistros(filtro);
            return Json(modelos);
        }

        [HttpPost]
        public ActionResult ver(string fac)
        {
            List<Object> tLineasFacturas = null;
            if (isLegal(fac))
            {
                TLineaFactura filtro = new TLineaFactura();
                filtro.CodFactura = fac;
                tLineasFacturas = control.Buscar(filtro);
            }
            return Json(tLineasFacturas);
        }

        private bool isLegal(string fac)
        {
            bool legal = false;

            if (fac != null)
            {
                if (((TUsuario)Session["usuario"]).Rol.Equals("Cliente"))
                {
                    TCliente cliente = new TCliente()
                    {
                        CodUsuario = ((TUsuario)Session["usuario"]).CodUsuario
                    };
                    cliente = (TCliente)control.Buscar(cliente)[0];
                    TFactura filtro = new TFactura();
                    filtro.CodFactura = fac;
                    TFactura factura = (TFactura)control.Buscar(filtro)[0];
                    if (factura.Cliente.Equals(cliente.CodCliente))
                    {
                        legal = true;
                    }
                    else
                    {
                        legal = false;
                    }
                }
                else if (((TUsuario)Session["usuario"]).Rol.Equals("Administrador"))
                {
                    legal = true;
                }
            }
            return legal;
        }

        public ActionResult add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult add(TFactura factura, List<TLineaFactura> lineaFacturas)
        {
            String mensaje;
            try
            {
                factura.CodFactura = UtilSQL.GenerarCodigo(factura.GetType());
                List<Object> facturas = new List<Object>();
                facturas.Add(factura);
                control.Insertar(facturas);
                mensaje = Mensajes.MSG_INSERTADO_CLIENTE;
            }
            catch (Exception e)
            {
                mensaje = "ERR" + e.ToString();
            }
            return Json(mensaje);
        }

        public PartialViewResult borrar(string fac)
        {
            TFactura filtro = new TFactura();
            filtro.CodFactura = fac;
            control.BorradoVirtual(control.Buscar(filtro));
            filtro = new TFactura();
            filtro.Borrado = "0";
            return PartialView("borrar", control.Buscar(filtro).Count);
        }
    }
}