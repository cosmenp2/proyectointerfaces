﻿using LibreriaV5_1_Final.Negocio;
using LibreriaV5_1_Final.Persistencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Tienda_V6
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            UtilFichero.RellenarDictionarySentencias();
        }

        protected void Application_End()
        {
            UtilFichero.EscribirDictionarySentenciasFichero();
            new ControlAccesoDAO<Object>().CerrarAplicacion();
        }
    }
}
