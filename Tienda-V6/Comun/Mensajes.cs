﻿using System;

namespace LibreriaV5_1_Final.Comun
{
    class Mensajes
    {
        public static String MSG_ARTICULO_NO_ENCONTRADO = "Articulo no encontrado";
        public static String MSG_INSERTADO_ARTICULO = "Articulo insertado correctamente";
        public static String MSG_BORRADO_ARTICULO = "Articulo borrado correctamente";
        public static String MSG_MODIFICADO_ARTICULO = "Articulo modificado correctamente";
        public static String MSG_YAEXISTE_ARTICULO = "Ese articulo ya existe";
        public static String MSG_SELECCIONAR_ARTICULO = "Seleccione un articulo";

        //------CLIENTES--------//

        public static String MSG_CLIENTE_NO_ENCONTRADO = "Cliente no encontrado";
        public static String MSG_INSERTADO_CLIENTE = "Se ha registrado correctamente";
        public static String MSG_BORRADO_CLIENTE = "Cliente borrado correctamente";
        public static String MSG_MODIFICADO_CLIENTE = "Sus datos se han modificado correctamente";
        public static String MSG_YAEXISTE_CLIENTE = "Ese cliente ya existe";
        public static String MSG_SELECCIONAR_CLIENTE = "Seleccione un cliente";

        //-------FACTURA--------//

        public static String MSG_PREGUNTA_BORRAR_FACTURA = "¿Estás seguro de borrar?";
        public static String MSG_INSERTAR_ELEMENTOS_FACTURA = "Añade elementos a la factura";
        //------GENERALES-------//
        public static String QUEST_BORRAR = "¿Deseas borrar el registro?";
        public static String MSG_CAMPOSVACIOS = "Debe rellenar los campos";
        public static String MSG_PAGINAS = "Número de páginas erróneo";
        public static String MSG_NO_CAMBIOS = "No se ha realizado ningún cambio";
        public static String MSG_PREGUNTA_BORRAR = "¿Desea borrarlo virtualmente?";
        public static String MSG_ATENCION = "¡Atención!";
        public static String MSG_BORRADO_VIRTUAL = "Borrado virtual satisfactorio";
        public static String MSG_LOGIN_EXITO = "Bienvenido";
        public static String MSG_LOGIN_ERROR = "Contraseña o email incorrecto";
        public static String MSG_LOGIN_ROLERROR = "No tienes permisos para acceder a esta web";
        public static String MSG_PASS_NO_IGUALES = "Las contraseñas no son iguales";
        public static String MSG_PASS_NO_CORRECTA = "La antigua contraseña es incorrecta";
        public static String MSG_PASS_CAMBIADA = "Se ha cambiado la contraseña con éxito";

        public static string mostrarmensaje(String mensaje, String pagina)
        {
            //Mostramos el mensaje de error
            return "<link rel = stylesheet href= /Content/css/mensaje.css>" +
                   "<div id = openModal class = modalDialog>" +
                   "<div>" +
                   "<a href= " + pagina + " title = Close class = 'close'>X</a>" +
                   "<h2> Mensaje </h2>" +
                   "<p>" + mensaje + "</p>" +
                   "</div>" +
                   "</div>";

        }
    }
}

