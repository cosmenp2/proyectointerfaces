﻿
namespace LibreriaV5_1_Final.Modelo
{
    public class TTipo
    {
        public string Tipo { get; set; }

        public TTipo()
        {
        }

        public TTipo(string tipo)
        {
            this.Tipo = tipo;
        }


    }
}
