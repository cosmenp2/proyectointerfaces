﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaV5_1_Final.Modelo
{
    public class TLineaFactura
    {

        public string CodFactura { get; set; }
        public string Articulo { get; set; }
        public string Cantidad { get; set; }
        public string Total { get; set; }

        public TLineaFactura(string codFactura, string articulo, string cantidad, string total)
        {
            CodFactura = codFactura;
            Articulo = articulo;
            Cantidad = cantidad;
            Total = total;
        }
        public TLineaFactura()
        {

        }

        public override string ToString()
        {
            return Articulo+" "+Cantidad+" "+Total;
        }
        public override bool Equals(object obj)
        {
            return ((TLineaFactura)obj).Articulo == Articulo;
        }
    }
}
