﻿using LibreriaV5_1_Final.Persistencia;

namespace LibreriaV5_1_Final.Modelo
{
    public class TArticulo
    {
        public string CodArticulo { get; set; }
        public string Fabricante { get; set; }
        public string Nombre { get; set; }
        public string Tipo { get; set; }
        public string Precio { get; set; }
        public string Estado { get; set; }
        public string Borrado { get; set; }
        
        public TArticulo(string codArticulo, string fabricante, string nombre, string tipo, string precio,string estado)
        {
            this.CodArticulo = codArticulo;
            this.Fabricante = fabricante;
            this.Nombre = nombre;
            this.Tipo = tipo;
            this.Precio = precio;
            this.Estado = estado;
        }
        public TArticulo(string fabrcante, string nombre, string tipo, string precio,string estado)
        {   
            this.CodArticulo =UtilSQL.GenerarCodigo(this.GetType());
       
            this.Fabricante = fabrcante;
            this.Nombre = nombre;
            this.Tipo = tipo;
            this.Precio = precio;
            this.Estado = estado;
            this.Borrado = "0";
        }
        public TArticulo() { }

        public override string ToString()
        {
            return CodArticulo+": " +Nombre.ToUpper();
        }

    }

    

}
