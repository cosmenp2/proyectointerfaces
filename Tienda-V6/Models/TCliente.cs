﻿using LibreriaV5_1_Final.Persistencia;

namespace LibreriaV5_1_Final.Modelo
{
    public class TCliente
    {
   
        public string CodCliente { get; set; }
        public string CodUsuario { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string DNI { get; set; }
        public string Direccion { get; set; }
        public string Borrado { get; set; }

        public TCliente(string codUsuario,string nombre, string apellidos, string dNI, string direccion)
        {   CodCliente= UtilSQL.GenerarCodigo(this.GetType());
            CodUsuario = codUsuario;
            Nombre = nombre;
            Apellidos = apellidos;
            DNI = dNI;
            Direccion = direccion;
           
            Borrado = "0";
        }

        public TCliente(string codCliente,string codUsuario, string nombre, string apellidos, string dNI, string direccion)
        {
            CodCliente = codCliente;
            CodUsuario = codUsuario;
            Nombre = nombre;
            Apellidos = apellidos;
            DNI = dNI;
            Direccion = direccion;
        }

        public TCliente()
        {

        }

        public override string ToString()
        {
            return CodCliente.ToLower()+": "+Nombre.ToUpper()+" "+Apellidos.ToUpper();
        }
    }
}
