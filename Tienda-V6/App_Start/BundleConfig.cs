﻿using System.Web;
using System.Web.Optimization;

namespace Tienda_V6
{
    public class BundleConfig
    {
        // Para obtener más información sobre las uniones, visite https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/LibreriaScripts").Include(
                "~/Scripts/jquery-{version}.js",
                "~/Scripts/jquery-ui-{version}.js",
                "~/Scripts/jquery.unobtrusive*",
                "~/Scripts/jquery.validate*",
                "~/Scripts/bootstrap-notify*",
                "~/Content/js/libreria.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información. De este modo, estará
            // para la producción, use la herramienta de compilación disponible en https://modernizr.com para seleccionar solo las pruebas que necesite.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));


            bundles.Add(new ScriptBundle("~/bundles/Carrito").Include(
                        "~/Scripts/Carrito/carrito.js"));

            bundles.Add(new ScriptBundle("~/bundles/ClienteAdd").Include(
                      "~/Scripts/Cliente/add.js"));

            bundles.Add(new ScriptBundle("~/bundles/ArticulosAdd").Include(
                      "~/Scripts/Articulo/add.js"));

            bundles.Add(new ScriptBundle("~/bundles/ArticulosCompra").Include(
                      "~/Scripts/Articulo/compra.js"));

            bundles.Add(new ScriptBundle("~/bundles/ArticulosConsulta").Include(
                     "~/Scripts/Articulo/Consultar.js"));
            bundles.Add(new ScriptBundle("~/bundles/ArticulosModificar").Include(
                     "~/Scripts/Articulo/modificar.js"));

            bundles.Add(new ScriptBundle("~/bundles/ClientesModificar").Include(
                     "~/Scripts/Cliente/modificar.js"));
            bundles.Add(new ScriptBundle("~/bundles/ClientesRestablecer").Include(
                     "~/Scripts/Cliente/restablecer.js"));
            bundles.Add(new ScriptBundle("~/bundles/ClientesConsultar").Include(
                     "~/Scripts/Cliente/consultar.js"));
            bundles.Add(new ScriptBundle("~/bundles/ClientesCerrarSesion").Include(
                     "~/Scripts/Cliente/cerrarSesion.js"));
            bundles.Add(new ScriptBundle("~/bundles/PedidosConsultar").Include(
         "~/Scripts/Pedido/consultar.js"));
            bundles.Add(new ScriptBundle("~/bundles/Plantilla").Include(
                     "~/Scripts/Plantilla/plantilla.js"));


        }
    }
}
