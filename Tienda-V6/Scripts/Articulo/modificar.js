﻿let $mensaje = document.querySelector('#mensaje');

function Enviar() {
    var post_url = '/Articulo/Modificar';
    var articulo = cargarArticulo(articulo);
    $.ajax({
        url: post_url,
        data: JSON.stringify(articulo),
        type: 'POST',
        success: exito,
        contentType: 'application/json'
    });
}

function exito(data) {
    if (data.substring(0, 3) == "ERR") {
        $mensaje.className = "alert alert-danger"
        $mensaje.textContent = data.substring(3);
    } else {
        $mensaje.className = "alert alert-success"
        $mensaje.textContent = data;
    }
}

function cargarArticulo() {
    var articulo = {};
    articulo.CodArticulo = document.getElementById("codArticulo").value;
    articulo.Nombre = document.getElementById("nombre").value;
    articulo.Fabricante = document.getElementById("fabricante").value;
    articulo.Tipo = document.getElementById("tipo").value;
    articulo.Precio = document.getElementById("precio").value;

    if (document.getElementById("nuevo").checked)
        articulo.Estado = document.getElementById("nuevo").value;
    else if (document.getElementById("seminuevo").checked)
        articulo.Estado = document.getElementById("seminuevo").value;
    else
        articulo.Estado = document.getElementById("terceraMano").value;
    articulo.Borrado = "0";
    return articulo;
}