﻿
/* El carro está compuesto por una matriz [i][j]
 * Si j=0 se accederá al articulo y
 * si j=1 se accederá a la línea del pedido
 */
let carrito;
let $mensaje = document.querySelector('#mensaje');

if (localStorage.carrito !== undefined) {
    carrito = JSON.parse(localStorage.carrito);
} else {
    carrito = []
}

function success(data) {
    addCarrito(data);
    localStorage.carrito = JSON.stringify(carrito);
    $mensaje.className = "alert alert-success";
    $mensaje.textContent = "Se ha añadido correctamente";
}

function addCarrito(dato) {
    let i = 0;
    let encontrado = false;

    while (!encontrado && i < carrito.length) {
        if (carrito[i][0]['CodArticulo'] == dato[0]['CodArticulo']) {
            encontrado = true;
            aumentarUnidades(i);
        }
        i++;
    }
    if (!encontrado) {
        carrito.push(dato);
    }
}

function aumentarUnidades(posicion) {
    carrito[posicion][1]['Cantidad'] = '' + (parseInt(carrito[posicion][1]['Cantidad']) + 1);
}