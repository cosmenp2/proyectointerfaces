﻿$(document).ready(function () {
    var paginas = $(".paginacion");

    console.log(paginas);

    for (pagina of paginas) {
        pagina.addEventListener('click', function (evt) {
            var hijo = evt.target;
            var numeroPagina = hijo.innerText;
            let paginaEnviar = {
                "numeroPagina": numeroPagina
            };
            var tabla = $("#tabla");
            tabla.remove();
            var uri = '/Articulo/buscarConPaginacion';
            $.ajax({
                url: uri,
                data: JSON.stringify(paginaEnviar),
                error: function (jqXHR) {
                    console.log(jqXHR);
                },
                success: exito,
                type: 'POST',
                contentType: 'application/json'
            });
        });
    }
});

function exito(data) {
    if (data != undefined) {
        var articulosPagina = $("#totalArticulos");
        var productos = data[0];
        var registros = data[1];
        articulosPagina.innerText = registros;
        var table = document.createElement("table");
        table.id = "tabla";
        table.setAttribute("class", "table");
        let thead = document.createElement('thead');
        let th2 = document.createElement('th');
        th2.textContent = 'Articulo';
        let th3 = document.createElement('th');
        th3.textContent = 'Borrar';
        let th4 = document.createElement('th');
        th4.textContent = 'Ver';
        let th5 = document.createElement('th');
        th5.textContent = 'Modificar';
        thead.appendChild(th2);
        thead.appendChild(th3);
        thead.appendChild(th4);
        thead.appendChild(th5);
        table.appendChild(thead);
        let tbody = document.createElement('tbody');

        for (let miItem of productos) {
            let tr = document.createElement('tr');
            tr.id = miItem['CodArticulo'];
            let td2 = document.createElement('td');
            console.log(miItem['Nombre']);
            td2.innerHTML = miItem['Nombre'];
            tr.appendChild(td2);
            //Hacer el onAction
            let td3 = document.createElement('td');
            td3.innerHTML = "<a class='btn btn-danger' data-ajax='true' data-ajax-method='POST' data-ajax-mode='replace'"
                + "data-ajax-update='#totalArticulos' href='/Articulo/borrar?art=" + miItem['CodArticulo'] + "' onclick='borrarArticulo(\"" + miItem['CodArticulo'] +"\")' >Borrar</a > ";
            tr.appendChild(td3);

            let td4 = document.createElement('td');
            
            td4.innerHTML = "<a  class='btn btn-success' href='/Articulo/ver?art="+miItem['CodArticulo']+"'>Ver</a>";
            tr.appendChild(td4);

            let td5 = document.createElement('td');
            td5.innerHTML = "<a class='btn btn-warning' href='/Articulo/modificar?art=" + miItem['CodArticulo'] +"'>Editar</a>";
            tr.appendChild(td5);

            tbody.appendChild(tr);
        }
        table.appendChild(tbody);
        document.getElementById("myList").appendChild(table);
        console.log("Realizada");   
    }   
}

function borrarArticulo(idElemento) {
    var list = document.getElementById(idElemento);

    if (list.hasChildNodes()) {
        list.parentNode.removeChild(list);
    }
}





