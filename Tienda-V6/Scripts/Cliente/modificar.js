﻿let $mensaje = document.querySelector('#mensaje');

function Enviar() {
    var post_url = '/Cliente/modificar';
    var cliente = cargarCliente(cliente);
    var usuario = cargarUsuario(usuario);
    var datos = {
        "usuario": usuario,
        "cliente": cliente
    }
    $.ajax({
        url: post_url,
        data: JSON.stringify(datos),
        type: 'POST',
        success: exito,
        contentType: 'application/json'
    });
}

function exito(data) {
    if (data.substring(0, 3) == "ERR") {
        $mensaje.className = "alert alert-danger"
        $mensaje.textContent = data.substring(3);
    } else {
        $mensaje.className = "alert alert-success"
        $mensaje.textContent = data;
    }
}

function cargarCliente() {
    var cliente = {};
    cliente.CodCliente = document.getElementById("codCliente").value;
    cliente.Nombre = document.getElementById("nombre").value;
    cliente.Apellidos = document.getElementById("apellidos").value;
    cliente.DNI = document.getElementById("dni").value;
    cliente.Direccion = document.getElementById("direccion").value;
    cliente.Borrado = "0";
    return cliente;
}
function cargarUsuario() {
    var Usuario = {};
    Usuario.Nick = document.getElementById("email").value;
    return Usuario;
}