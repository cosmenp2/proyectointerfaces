﻿let $mensaje = document.querySelector('#mensaje');
let varIsAdmin = false;

function Enviar() {
    //var post_url = '@Url.Action("add", "Cliente")';
    var post_url= '/Cliente/add'
    var cliente = cargarCliente(cliente);
    var usuario = cargarUsuario(usuario);
    var datos = {
        "usuario": usuario,
        "cliente": cliente
    };
    $.ajax({
        url: post_url,
        data: JSON.stringify(datos),
        type: 'POST',
        success: exito,
        contentType: 'application/json'
    });
}

function exito(data) {
    
    if (data.substring(0, 3) == "ERR") {
        $mensaje.className = "alert alert-danger";
        $mensaje.textContent = data.substring(3);
    } else {
        $mensaje.className = "alert alert-success";
        $mensaje.textContent = data;
        document.getElementById("form").reset();
        if (!varIsAdmin) {
            window.location = "/";
        
        }
    }
}

function cargarCliente() {

    var cliente = {};
    cliente.Nombre = document.getElementById("nombre").value;
    cliente.Apellidos = document.getElementById("apellidos").value;
    cliente.DNI = document.getElementById("dni").value;
    cliente.Direccion = document.getElementById("direccion").value;
    cliente.Borrado = "0";
    return cliente;
}

function cargarUsuario() {
    var Usuario = {};
    Usuario.Nick = document.getElementById("email").value;
    Usuario.Pass = document.getElementById("password").value;
    return Usuario;
}

//var uri = '@Url.Action("ComprobarExistenciaEmail", "Cliente")';
var uri = '/Cliente/ComprobarExistenciaEmail';
$(document).ready(function () {
    $('#email').on('blur', function () {
        $('#result-email').html('<img src="http://moviedickapk.epizy.com/loader.gif" />').fadeOut(1000);

        var email = { "email": $(this).val() }
        $.ajax({
            type: "POST",
            url: uri,
            data: JSON.stringify(email),
            contentType: 'application/json',
            success: function (data) {
                $('#result-email').fadeIn(1000).html(data);
            }
        });
    });
});

function isAdmin() {
    //var post_url = '@Url.Action("add", "Cliente")';
    var post_url = '/Cliente/ComprobarSession'
    $.ajax({
        url: post_url,
        type: 'POST',
        success: exitoAdmin,
        contentType: 'application/json'
    });
}

function exitoAdmin(data) {
    varIsAdmin = data;
    Enviar();
}