﻿let $mensaje = document.querySelector('#mensaje');

function Enviar() {
    var post_url = '/Cliente/restablecer';
    var datos = cargarDatos();
    $.ajax({
        url: post_url,
        data: JSON.stringify(datos),
        type: 'POST',
        success: exito,
        contentType: 'application/json'
    });
}

function exito(data) {
    if (data.substring(0, 3) == "ERR") {
        $mensaje.className = "alert alert-danger"
        $mensaje.textContent = data.substring(3);
    } else {
        $mensaje.className = "alert alert-success"
        $mensaje.textContent = data;
    }
}

function cargarDatos() {
    var data = {
        "oldPass": document.getElementById("oldPass").value,
        "pass1": document.getElementById("pass1").value,
        "pass2": document.getElementById("pass2").value
    }
    return data;
}