﻿let $mensajelogin = document.querySelector('#mensajelogin');

function EnviarLogin() {
    var post_url = '/cliente/login';
    var usuario = cargarUsuarioLogin(usuario);
    $.ajax({
        url: post_url,
        data: JSON.stringify(usuario),
        type: 'POST',
        success: exitoLogin,
        contentType: 'application/json'
    });
}

function exitoLogin(data) {
    localStorage.removeItem("carrito");
    if (data.substring(0, 3) == "ERR") {
        $mensajelogin.className = "alert alert-danger";
        $mensajelogin.textContent = data.substring(3);
    } else {
        window.location = "/";
    }
}

function cargarUsuarioLogin() {
    var usuario = {};
    usuario.Nick = document.getElementById("emailLogin").value;
    usuario.Pass = document.getElementById("passwordLogin").value;
    return usuario;
}