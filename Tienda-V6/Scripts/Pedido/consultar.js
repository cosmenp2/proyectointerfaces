﻿$(document).ready(function () {
    var paginas = $(".pagination");

    console.log(paginas);

    for (pagina of paginas) {
        pagina.addEventListener('click', function (evt) {
            var hijo = evt.target;
            var numeroPagina = hijo.innerText;
            let paginaEnviar = {
                "numeroPagina": numeroPagina
            };
            var tabla = $("#tabla");
            tabla.remove();
            var uri = '/Pedido/consultarConPaginacion';
            $.ajax({
                url: uri,
                data: JSON.stringify(paginaEnviar),
                error: function (jqXHR) {
                    console.log(jqXHR);
                },
                success: renderizarPagina,
                type: 'POST',
                contentType: 'application/json'
            });
        });
    }
});

let $modal = document.querySelector('#container_lineas');

function renderizarPagina(data) {
    if (data != undefined) {
        var pedidosPagina = $("#totalPedidos");
        var pedidos = data[0];
        var registros = data[1];
        pedidosPagina.innerText = registros;
        var table = document.createElement("table");
        table.id = "tabla";
        table.setAttribute("class", "table");
        let thead = document.createElement('thead');
        let th2 = document.createElement('th');
        th2.textContent = 'Nombre';
        let th3 = document.createElement('th');
        th3.textContent = 'Fecha';
        let th4 = document.createElement('th');
        th4.textContent = 'Ver';
        thead.appendChild(th2);
        thead.appendChild(th3);
        thead.appendChild(th4);
        table.appendChild(thead);
        let tbody = document.createElement('tbody');

        for (let miItem of pedidos) {
            let tr = document.createElement('tr');
            tr.id = miItem['CodFactura'];
            let td1 = document.createElement('td');
            td1.innerHTML = miItem['CodFactura'];
            tr.appendChild(td1);
            let td2 = document.createElement('td');
            td2.innerHTML = miItem['FechaFactura'];
            tr.appendChild(td2);
            //Hacer el onAction
            let td3 = document.createElement('td');
            td3.innerHTML = "<a class='btn btn-success pop-up' data-ajax='true' data-ajax-method='POST' data-toggle='modal' data-target='#emergente'"
                + "data-ajax-success='exito' data-ajax-mode='replace' href='/Pedido/ver?fac=" + miItem['CodFactura'] + "'>Ver pedido</a> ";
            tr.appendChild(td3);

            tbody.appendChild(tr);
        }
        table.appendChild(tbody);
        document.getElementById("myList").appendChild(table);
        console.log("Realizada");
    }
}

function exito(data) {
    document.querySelector('#model').textContent = 'Pedido ' + data[0]['CodFactura'];
    rellenarAlert(data);
}

function limpiarAlert() {
    for (var i = $modal.childNodes.length - 1; i >= 0; i--) {
        $modal.removeChild($modal.childNodes[i]);
    }
}

function rellenarAlert(data) {
    limpiarAlert();
    var indice = 0;

    if (data.length != 0) {
        let tabla = document.createElement('table');
        tabla.style.width = '100%';
        tabla.classList.add('table', 'table-striped', 'table-bordered', 'table-hover');
        let thead = document.createElement('thead');
        let th2 = document.createElement('th');
        th2.textContent = 'Producto';
        let th3 = document.createElement('th');
        th3.textContent = 'Precio';
        let th4 = document.createElement('th');
        th4.textContent = 'Unidades';
        let th5 = document.createElement('th');
        th5.textContent = 'Subtotal';
        thead.appendChild(th2);
        thead.appendChild(th3);
        thead.appendChild(th4);
        thead.appendChild(th5);
        tabla.appendChild(thead);

        let tbody = document.createElement('tbody');

        for (let miItem of data) {
            let tr = document.createElement('tr');
            let td2 = document.createElement('td');
            td2.textContent = `${miItem['Articulo']}`;
            tr.appendChild(td2);

            let td3 = document.createElement('td');
            td3.align = 'right';
            td3.textContent = `${parseInt(miItem['Total']) / parseInt(miItem['Cantidad'])}`;
            tr.appendChild(td3);

            let td4 = document.createElement('td');
            td4.textContent = miItem['Cantidad'] + " ";
            tr.appendChild(td4);

            let td5 = document.createElement('td');
            td5.align = 'right';
            let strongSub = document.createElement('strong');
            let spanSub = document.createElement('span');
            spanSub.classList.add('subt');
            let subtotal = parseInt(miItem['Total']);
            spanSub.textContent = "" + subtotal;
            strongSub.appendChild(spanSub);
            td5.appendChild(strongSub);
            tr.appendChild(td5);

            tbody.appendChild(tr);
            indice = indice + 1;
        }
        let tr2 = document.createElement('tr');
        let td1 = document.createElement('td');
        td1.colSpan = '3';
        let span1 = document.createElement('span');
        span1.classList.add('pull-right');
        let strong1 = document.createElement('strong');
        strong1.textContent = 'Total';
        span1.appendChild(strong1);
        td1.appendChild(span1);
        tr2.appendChild(td1);

        let td2 = document.createElement('td');
        td2.align = 'right';
        let span2 = document.createElement('span');
        let strong2 = document.createElement('strong');
        let total = calcularTotal(data);
        strong2.textContent = '' + total;
        span2.appendChild(strong2);
        td2.appendChild(span2);
        tr2.appendChild(td2);

        tbody.appendChild(tr2);
        tabla.appendChild(tbody);
        $modal.appendChild(tabla);


    }
}

function calcularTotal(data) {
    total = 0;
    for (let miItem of data) {
        total = total + (parseInt(miItem['Total']));
    }
    let totalDosDecimales = total.toFixed(2);
    return totalDosDecimales;
}