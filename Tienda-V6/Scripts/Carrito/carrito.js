﻿let total = 0;
let $carrito = document.querySelector('#carrito');
let $buyButton = document.querySelector('#confirm');
let $mensaje = document.querySelector('#mensaje');
let carrito;

if (localStorage.carrito !== undefined) {
    carrito = JSON.parse(localStorage.carrito);
    renderizarCarrito();
} else {
    renderizarVacio();
}


function renderizarCarrito() {
    limpiarPantalla();
    var indice = 0;

    if (carrito.length != 0) {
        let tabla = document.createElement('table');
        tabla.style.width = '100%';
        tabla.classList.add('table', 'table-striped', 'table-bordered', 'table-hover');
        let thead = document.createElement('thead');
        let th1 = document.createElement('th');
        let th2 = document.createElement('th');
        th2.textContent = 'Producto';
        let th3 = document.createElement('th');
        th3.textContent = 'Precio';
        let th4 = document.createElement('th');
        th4.textContent = 'Unidades';
        let th5 = document.createElement('th');
        th5.textContent = 'Subtotal';
        thead.appendChild(th1);
        thead.appendChild(th2);
        thead.appendChild(th3);
        thead.appendChild(th4);
        thead.appendChild(th5);
        tabla.appendChild(thead);

        let tbody = document.createElement('tbody');

        for (let miItem of carrito) {
            let tr = document.createElement('tr');
            let td1 = document.createElement('td');
            let btDelete = document.createElement('button');
            btDelete.type = 'button';
            btDelete.classList.add('btn', 'btn-danger', 'btn-sm', 'remove_prod');
            btDelete.addEventListener('click', borrarItemCarrito);
            btDelete.setAttribute('posicion', indice);
            let iTagDelete = document.createElement('i');
            iTagDelete.classList.add('fa', 'fa-trash', 'fa-fw');
            btDelete.appendChild(iTagDelete);
            btDelete.textContent = 'Eliminar';
            td1.appendChild(btDelete);
            tr.appendChild(td1);

            let td2 = document.createElement('td');
            td2.textContent = `${miItem[0]['Nombre']}`;
            tr.appendChild(td2);

            let td3 = document.createElement('td');
            td3.align = 'right';
            td3.textContent = `${miItem[0]['Precio']}`;
            tr.appendChild(td3);

            let td4 = document.createElement('td');
            let btMinus = document.createElement('button');
            btMinus.type = 'button';
            btMinus.textContent = '-';
            btMinus.classList.add('btn', 'btn-warning', 'btn-sm', 'minus_qty2');
            btMinus.addEventListener('click', reducirUnidades);
            btMinus.setAttribute('posicion', indice);
            let iTagMinus = document.createElement('i');
            iTagMinus.classList.add('fa', 'fa-minus', 'fa-fw');
            btMinus.appendChild(iTagMinus);
            let btMore = document.createElement('button');
            btMore.type = 'button';
            btMore.textContent = '+';
            btMore.classList.add('btn', 'btn-primary', 'btn-sm', 'add_qty2');
            btMore.addEventListener('click', aumentarUnidades);
            btMore.setAttribute('posicion', indice);
            let iTagMore = document.createElement('i');
            iTagMore.classList.add('fa', 'fa-plus', 'fa-fw');
            btMore.appendChild(iTagMore);
            td4.textContent = miItem[1]['Cantidad'] + " ";
            td4.appendChild(btMinus);
            td4.appendChild(btMore);
            tr.appendChild(td4);

            let td5 = document.createElement('td');
            td5.align = 'right';
            let strongSub = document.createElement('strong');
            let spanSub = document.createElement('span');
            spanSub.classList.add('subt');
            let subtotal = parseInt(miItem[0]['Precio']) * parseInt(miItem[1]['Cantidad']);
            spanSub.textContent = "" + subtotal;
            strongSub.appendChild(spanSub);
            td5.appendChild(strongSub);
            tr.appendChild(td5);

            tbody.appendChild(tr);
            indice = indice + 1;
        }
        let tr2 = document.createElement('tr');
        let td1 = document.createElement('td');
        td1.colSpan = '4';
        let span1 = document.createElement('span');
        span1.classList.add('pull-right');
        let strong1 = document.createElement('strong');
        strong1.textContent = 'Total';
        span1.appendChild(strong1);
        td1.appendChild(span1);
        tr2.appendChild(td1);

        let td2 = document.createElement('td');
        td2.align = 'right';
        let span2 = document.createElement('span');
        let strong2 = document.createElement('strong');
        let total = calcularTotal();
        strong2.textContent = '' + total;
        span2.appendChild(strong2);
        td2.appendChild(span2);
        tr2.appendChild(td2);

        tbody.appendChild(tr2);
        tabla.appendChild(tbody);
        $carrito.appendChild(tabla);

        let buyButton = document.createElement('button');
        buyButton.type = 'button';
        buyButton.classList.add('btn', 'btn-primary', 'pull-right');
        buyButton.style.marginRight = '15px';
        buyButton.addEventListener('click', comprar);
        let iTagBuy = document.createElement('i');
        iTagBuy.classList.add('fa', 'fa-check', 'fa-fw');
        buyButton.appendChild(iTagBuy);
        buyButton.textContent = 'Comprar';
        $buyButton.appendChild(buyButton);
    } else {
        renderizarVacio();
    }
}

function limpiarPantalla() {
    for (var i = $carrito.childNodes.length - 1; i >= 0; i--) {
        $carrito.removeChild($carrito.childNodes[i]);
    }
    for (var i = $buyButton.childNodes.length - 1; i >= 0; i--) {
        $buyButton.removeChild($buyButton.childNodes[i]);
    }
}

function renderizarVacio() {
    limpiarPantalla();
    let h1 = document.createElement('h1');
    h1.classList.add('display-4');
    h1.textContent = 'No hay ningún artículo';
    $carrito.appendChild(h1);
    $buyButton.textContent = '';
}

function calcularTotal() {
    total = 0;
    for (let miItem of carrito) {
        total = total + (parseInt(miItem[0]['Precio']) * parseInt(miItem[1]['Cantidad']));
    }
    let totalDosDecimales = total.toFixed(2);
    return totalDosDecimales;
}

function borrarItemCarrito(pos = undefined) {
    let posicion;
    if (pos === undefined) {
        posicion = this.getAttribute('posicion');
    } else {
        posicion = pos;
    }
    carrito.splice(posicion, 1);
    localStorage.removeItem("carrito");
    localStorage.carrito = JSON.stringify(carrito);
    renderizarCarrito(carrito);
}

function aumentarUnidades() {
    let posicion = this.getAttribute('posicion');
    carrito[posicion][1]['Cantidad'] = '' + (parseInt(carrito[posicion][1]['Cantidad']) + 1);
    localStorage.removeItem("carrito");
    localStorage.carrito = JSON.stringify(carrito);
    renderizarCarrito();
}

function reducirUnidades() {
    let posicion = this.getAttribute('posicion');
    carrito[posicion][1]['Cantidad'] = '' + (parseInt(carrito[posicion][1]['Cantidad']) - 1);
    if (parseInt(carrito[posicion][1]['Cantidad']) <= 0) {
        borrarItemCarrito(posicion);
    } else {
        localStorage.removeItem("carrito");
        localStorage.carrito = JSON.stringify(carrito);
        renderizarCarrito();
    }
}

function comprar() {
    //var uri = '@Url.Action("comprar", "Carrito")';
    //Ya que aqui esto no se auto completa, debemos poner la uri a mano.
    var uri = '/Carrito/comprar';
    let lineas = [];

    for (var i = 0; i < carrito.length; i++) {
        lineas.push(carrito[i][1]);
    }

    $.ajax({
        url: uri,
        data: JSON.stringify(lineas),
        type: 'POST',
        success: exito,
        error: fallo,
        contentType: 'application/json'
    });
}

function exito(data) {
    $mensaje.textContent = data;
    if (data.substring(0, 3) == "ERR") {
        $mensaje.className = "alert alert-danger";
        $mensaje.textContent = data.substring(3);
    } else {
        $mensaje.className = "alert alert-success";
        $mensaje.textContent = data;
        renderizarVacio();
        localStorage.removeItem("carrito");
        carrito = [];
    }
}

function fallo() {
        $mensaje.className = "alert alert-danger";
        $mensaje.textContent = "Error no esta identificado";
    
}