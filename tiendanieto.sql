-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 17-01-2020 a las 12:13:21
-- Versión del servidor: 10.4.6-MariaDB
-- Versión de PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tiendanieto`
--
CREATE DATABASE IF NOT EXISTS `tiendanieto` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `tiendanieto`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarticulo`
--

CREATE TABLE `tarticulo` (
  `CodArticulo` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Fabricante` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Tipo` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Precio` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Estado` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Borrado` varchar(1) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `tarticulo`
--

INSERT INTO `tarticulo` (`CodArticulo`, `Fabricante`, `Nombre`, `Tipo`, `Precio`, `Estado`, `Borrado`) VALUES
('cod001', 'DorrLaco', 'Puerta', 'Muebles', '23', 'nuevo', '1'),
('cod002', 'DorrLacos', 'Silla', 'Muebles', '10', 'nuevo', '1'),
('cod003', 'Siemens', 'Lavadora', 'Electrodomestico', '399,99', 'seminuevo', '0'),
('cod004', 'asdasd', 'asa', 'Electronica', '234', 'seminuevo', '1'),
('cod005', 'Beko', 'Prueba Añadir', 'Electrodomestico', '123', 'nuevo', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tcliente`
--

CREATE TABLE `tcliente` (
  `CodCliente` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CodUsuario` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Apellidos` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `DNI` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Direccion` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Borrado` varchar(1) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `tcliente`
--

INSERT INTO `tcliente` (`CodCliente`, `CodUsuario`, `Nombre`, `Apellidos`, `DNI`, `Direccion`, `Borrado`) VALUES
('cod001', 'cod001', 'Cosme', 'Nieto Perez', '123456', 'asfdq', '0'),
('cod002', 'cod002', 'pep', 'pepe lopez', '123123', 'asdfgh', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tfactura`
--

CREATE TABLE `tfactura` (
  `CodFactura` varchar(10) NOT NULL,
  `Cliente` varchar(10) NOT NULL,
  `FechaFactura` varchar(15) NOT NULL,
  `Borrado` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tlineafactura`
--

CREATE TABLE `tlineafactura` (
  `CodFactura` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Articulo` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Cantidad` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Total` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ttipo`
--

CREATE TABLE `ttipo` (
  `Tipo` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `Borrado` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `ttipo`
--

INSERT INTO `ttipo` (`Tipo`, `Borrado`) VALUES
('Electrodomestico', 0),
('Electronica', 0),
('Mascotas', 0),
('Muebles', 0),
('Otros', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tusuario`
--

CREATE TABLE `tusuario` (
  `CodUsuario` varchar(50) NOT NULL,
  `Nick` varchar(50) DEFAULT NULL,
  `Pass` varchar(249) DEFAULT NULL,
  `Rol` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tusuario`
--

INSERT INTO `tusuario` (`CodUsuario`, `Nick`, `Pass`, `Rol`) VALUES
('cod001', 'cosmenp@gmail.com', 'sbN3OgXA7QF2eHpPFXT/AHX3Uh4=', 'Administrador'),
('cod002', 'pepe@gmail.com', '8baZzJrz7rmOXeJEyngCrjjne64=', 'Cliente');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tarticulo`
--
ALTER TABLE `tarticulo`
  ADD PRIMARY KEY (`CodArticulo`);

--
-- Indices de la tabla `tcliente`
--
ALTER TABLE `tcliente`
  ADD PRIMARY KEY (`CodCliente`);

--
-- Indices de la tabla `tfactura`
--
ALTER TABLE `tfactura`
  ADD PRIMARY KEY (`CodFactura`);

--
-- Indices de la tabla `tlineafactura`
--
ALTER TABLE `tlineafactura`
  ADD PRIMARY KEY (`CodFactura`,`Articulo`);

--
-- Indices de la tabla `ttipo`
--
ALTER TABLE `ttipo`
  ADD PRIMARY KEY (`Tipo`),
  ADD UNIQUE KEY `tema` (`Tipo`);

--
-- Indices de la tabla `tusuario`
--
ALTER TABLE `tusuario`
  ADD PRIMARY KEY (`CodUsuario`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
